ARG TARGET

# build environment
FROM ghcr.io/cross-rs/$TARGET:latest

RUN \
  apt update && \
  apt upgrade -y && \
  apt install -y libclang-dev protobuf-compiler

ARG UID=991
ARG GID=991
ARG TARGET
ARG TOOL
ARG BUILD_MODE=release

ENV \
  UID=${UID} \
  GID=${GID} \
  TARGET=${TARGET} \
  TOOL=${TOOL} \
  BUILD_MODE=${BUILD_MODE} \
  TOOLCHAIN=stable \
  CC=${TOOL}-cc \
  CXX=${TOOL}-c++ \
  BINDGEN_EXTRA_CLANG_ARGS="--sysroot=/usr/local/$TOOL/ --target=$TARGET -L/usr/local/$TOOL/lib -I/usr/local/$TOOL/include --static -static-libgcc -static-libstdc++" \
  HOST_CC=cc

RUN \
  addgroup --gid "${GID}" build && \
  adduser \
  --disabled-password \
  --gecos "" \
  --ingroup build \
  --uid "${UID}" \
  --home /opt/build \
  build

ADD \
  https://sh.rustup.rs /opt/build/rustup.sh

RUN \
  chown -R build:build /opt/build

USER build
WORKDIR /opt/build

ENV \
  PATH=/opt/build/.cargo/bin:/usr/local/musl/bin:/usr/local/sbin:/usr/local/bin:/usr/bin:/sbin:/bin

RUN \
  chmod +x rustup.sh && \
  ./rustup.sh --default-toolchain $TOOLCHAIN --profile minimal -y && \
  rustup target add $TARGET

USER build

WORKDIR /opt/build/

COPY root/ /

CMD /usr/bin/bash
